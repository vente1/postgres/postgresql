FROM mdillon/postgis:11-alpine

LABEL maintainer="info@redmic.es"

ENV PG_CRON_VERSION="1.2.0" \
	PG_PARTMAN_VERSION="4.2.2" \
	PG_PGROUTING_VERSION="2.6.3" \
	POSTGRES_WORK_MEM="1GB" \
	CGAL_VERSION="4.14.2"

RUN apk add --no-cache --virtual \
		.build-deps \
		build-base=0.5-r1 \
		cmake=~3.13 \
		ca-certificates=~20191127 \
		openssl=~1.1 \
		perl=~5.26 \
		boost=~1.67 \
		boost-dev=~1.67 \
		tar=~1 \
        gmp=~6.1 \
        gmp-dev=~6.1 \
        mpfr-dev=~3.1 \
        xz=~5.2

# hadolint ignore=DL3003
RUN wget -O cgal.tar.xz https://github.com/CGAL/cgal/releases/download/releases%2FCGAL-${CGAL_VERSION}/CGAL-${CGAL_VERSION}.tar.xz && \
    mkdir -p /usr/src/cgal && \
    tar --extract --file cgal.tar.xz --directory /usr/src/cgal --strip-components 1 && \
    rm cgal.tar.xz && \
    cd /usr/src/cgal && \
    cmake -DCMAKE_INSTALL_LIBDIR='/usr/local/lib' . && \
    make && \
    make install && \
    cd / && \
    rm -rf /usr/src/cgal && \
	wget -O pg_cron.tgz https://github.com/citusdata/pg_cron/archive/v${PG_CRON_VERSION}.tar.gz && \
	tar xvzf pg_cron.tgz && \
	cd pg_cron-${PG_CRON_VERSION} && \
	sed -i.bak -e 's/-Werror//g' Makefile && \
	sed -i.bak -e 's/-Wno-implicit-fallthrough//g' Makefile && \
	make && \
	make install && \
	cd / && \
	rm -rf pg_cron.tgz pg_cron-* && \
	wget -O pg_partman.tgz https://github.com/pgpartman/pg_partman/archive/v${PG_PARTMAN_VERSION}.tar.gz && \
	tar xvzf pg_partman.tgz && \
	cd pg_partman-${PG_PARTMAN_VERSION} && \
	make && \
	make NO_BGW=1 install && \
	cd / && \
	rm -rf pg_partman.tgz pg_partman-* && \
	wget -O pgrouting.tgz https://github.com/pgRouting/pgrouting/archive/v${PG_PGROUTING_VERSION}.tar.gz && \
	tar xvzf pgrouting.tgz && \
	cd pgrouting-${PG_PGROUTING_VERSION} && \
	mkdir build && \
	cd build && \
	cmake .. && \
	make && \
	make install && \
	cd / && \
	rm -rf pgrouting.tgz pgrouting-* && \
	echo "shared_preload_libraries='pg_cron'" >> /usr/local/share/postgresql/postgresql.conf.sample && \
	echo "checkpoint_timeout = 30min" >> /usr/local/share/postgresql/postgresql.conf.sample && \
	echo "max_wal_size = 2GB" >> /usr/local/share/postgresql/postgresql.conf.sample && \
	mv /usr/local/bin/docker-entrypoint.sh /usr/local/bin/docker-entrypoint-origin.sh

COPY rootfs /
